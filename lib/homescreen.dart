import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'model.dart';
import 'actions.dart';
import 'historyscreen.dart';

class HomeScreen extends StatelessWidget {

  Store<MainState> store;

  HomeScreen(this.store);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text(store.state.title, style: new TextStyle(color: Colors.black),),
          actions: <Widget>[
            new IconButton(
                icon: new Icon(
                    Icons.history,
                    color: Colors.black),
                onPressed: () => Navigator.push(context, new MaterialPageRoute(
                    builder: (BuildContext context) => new HistoryScreen(context, store)))
            ),
            new IconButton(
              icon: new Icon(
                  Icons.save,
                  color: Colors.black
              ),
              tooltip: 'Save Day',
              onPressed: () => store.dispatch(new SaveCountAction(store.state.count)),
            )
          ],
          backgroundColor: Colors.lime,
        ),
        body: incrementWidget(store),
        floatingActionButton: new FloatingActionButton(
          onPressed: () => store.dispatch(new IncrementCountAction(store.state.count)),
          tooltip: 'Increment',
          child: new Icon(Icons.add, color: Colors.black),
          backgroundColor: Colors.lime,
        )
    );
  }
}

Widget incrementWidget(store) {
  int count = store.state.count;
  Color bgColor = store.state.color;

  return Container(
      color: Colors.black,
      child: new Center(child: new CircleAvatar(
          maxRadius: 180.0,
          backgroundColor: bgColor,
          child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Text('You have smoked:',
                    style: new TextStyle(color: Colors.black)),
                new Text('$count',
                    style: new TextStyle(fontSize: 100.0, color: Colors.black)),
                new Text('cigarettes',
                    style: new TextStyle(color: Colors.black)),
                new Text('Today',
                    style: new TextStyle(fontSize: 40.0, color: Colors.black)
                )
              ]
          )
      ))
  );
}