
class IncrementCountAction {

  final int count;

  IncrementCountAction(this.count);
}

class DecrementCountAction {

  final int count;

  DecrementCountAction(this.count);
}

class SaveCountAction {

  final int count;

  SaveCountAction(this.count);
}

class DeleteHistoryAction {}