import 'package:flutter/material.dart';
import 'actions.dart';
import 'model.dart';

MainState mainReducer(MainState state, dynamic action) {
  if (action is IncrementCountAction) {
    return incrementCount(state, action);
  } else if (action is DecrementCountAction) {
    return decrementCount(state, action);
  } else if (action is SaveCountAction) {
    return saveCount(state, action);
  } else if (action is DeleteHistoryAction) {
    return deleteHistory(state, action);
  }
  return state;
}

MainState incrementCount(MainState state, IncrementCountAction action) {
  int newCount = state.count + 1;
  Color newColor = state.getColor(newCount);
  Smoke smoke = new Smoke(new Marlboro(6.0, 19), new DateTime.now());
  state.history.smokes.add(smoke);

  print('new count: $newCount, new color: $newColor, new Smoke: $smoke');

  MainState newState = new MainState(
    state.title,
    newCount,
    newColor,
    state.history,
  );
  return newState;
}

MainState decrementCount(MainState state, DecrementCountAction action) {
  int newCount = state.count - 1;
  Color newColor = state.getColor(newCount);
  state.history.smokes.removeLast();

  MainState newState = new MainState(
      state.title,
      newCount,
      newColor,
      state.history
  );
  return newState;
}

MainState saveCount(MainState state, SaveCountAction action) {
  DayHistory dayHistory = new DayHistory(state.count, DateTime.now(), state.color);
  state.history.days.add(dayHistory);
  int emptyCount = 0;
  Color emptyColor = Colors.lightGreen;

  return new MainState(
    state.title,
    emptyCount,
    emptyColor,
    state.history
  );
}

MainState deleteHistory(MainState state, DeleteHistoryAction action) {
  History emptyHistory = new History(new List(), new List());

  return new MainState(
    state.title,
    state.count,
    state.color,
    emptyHistory
  );
}