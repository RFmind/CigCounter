import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'homescreen.dart';
import 'reducers.dart';
import 'model.dart';

void main() => runApp(CigApp());

class CigApp extends StatelessWidget {

  var store = new Store<MainState>(
    mainReducer,
    initialState: MainState.init
  );

  @override
  Widget build(BuildContext context) {
    return new StoreProvider<MainState>(
      store: store,
      child: new MaterialApp(
        title: 'Cigarette Counter',
        home: new StoreBuilder<MainState>(
            builder: (context, store) => new HomeScreen(store))
      )
    );
  }
}