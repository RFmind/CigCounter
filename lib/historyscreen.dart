import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'model.dart';
import 'actions.dart';

class HistoryScreen extends StatelessWidget {

  Store<MainState> store;

  HistoryScreen(context, this.store);

  String _weekday(int dayNumber) {
    switch(dayNumber) {
      case 1:
        return "Monday";
      case 2:
        return "Tuesday";
      case 3:
        return "Wednesday";
      case 4:
        return "Thursday";
      case 5:
        return "Friday";
      case 6:
        return "Saturday";
      case 7:
        return "Sunday";
    }
  }

  List<Widget> buildList() {
    List<Widget> listview = new List();

    for (var dayHistory in store.state.history.days) {
      var day = dayHistory.date.day;
      var month = dayHistory.date.month;
      var year = dayHistory.date.year;
      var weekday = _weekday(dayHistory.date.weekday);
      var count = dayHistory.count;

      listview.add(
        new ListTile(
          leading: new CircleAvatar(backgroundColor: dayHistory.color),
          title: new Text('$weekday $day / $month / $year - count: $count', style: new TextStyle(color: Colors.white))
        )
      );
    }

    if (listview.isEmpty) {
      listview.add(
        new ListTile(
          leading: new CircleAvatar(backgroundColor: Colors.black),
          title: new Text('No data yet.. please create some :)', style: new TextStyle(color: Colors.white))
        )
      );
    }

    return listview;
  }

  void _deleteHistory(BuildContext context) {
    store.dispatch(new DeleteHistoryAction());
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {

    return new Scaffold(
      appBar: new AppBar(
        title: new Text(store.state.title),
        backgroundColor: Colors.lime,
        textTheme: new TextTheme(
          title: new TextStyle(color: Colors.black, fontSize: 20.0, fontWeight: FontWeight.bold)
        ),
        iconTheme: new IconThemeData(color: Colors.black),
        actions: <Widget>[
          new IconButton(
              icon: new Icon(Icons.delete_forever),
              onPressed: () => _deleteHistory(context)
          )
        ]),
      body: new Container(
        color: Colors.black,
        child: new Column(
          children: buildList()
        )
      )
    );
  }
}