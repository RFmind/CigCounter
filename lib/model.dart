import 'package:flutter/material.dart';

class Brand {

  double price;
  int cigCount; // cigsCount per pack

  Brand(this.price, this.cigCount);

  double getPricePerCig() {
    return price / cigCount;
  }
}

class Marlboro extends Brand {

  Marlboro(double price, int cigCount) : super(price, cigCount);
}

class PallMall extends Brand {

  PallMall(double price, int cigCount) : super(price, cigCount);
}

class Smoke {
  Brand brand;
  DateTime dateTime;

  Smoke(this.brand, this.dateTime);

  double getCost() {
    return brand.getPricePerCig();
  }
}

class DayHistory {
  int count;
  Color color;
  DateTime date;

  DayHistory(this.count, this.date, this.color);
}

class History {
  List<Smoke> smokes;
  List<DayHistory> days;

  History(this.smokes, this.days);

  static final empty = new History(new List(), new List());
}


class MainState {
  String title;
  int count;
  Color color;
  History history;

  MainState(this.title, this.count, this.color, this.history);

  static var init = MainState(
    'Cigarette Counter',
    0,
    Colors.lightGreen,
    History.empty
  );

  Color getColor(int count) {
    if (count < 5) {
      return Colors.lightGreen;
    } else if (count < 10) {
      return Colors.yellow;
    } else if (count < 19) {
      return Colors.orange;
    } else {
      return Colors.red;
    }
  }
}
